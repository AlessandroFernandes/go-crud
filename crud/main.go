package main

import (
	"crud/router"
	"crud/util"
	"net/http"
)

func main() {
	router.SendRoute()
	http.ListenAndServe(":"+util.GetEnvVariable("PORT"), nil)
}
