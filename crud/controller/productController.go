package controller

import (
	"crud/config"
	"crud/database"
	"crud/model"
	"net/http"
	"strconv"
)

var tmpt = config.Tmpt

func Index(resp http.ResponseWriter, req *http.Request) {
	products := database.GetProducts()
	wd := model.WebData{
		Title:    "Welcome to the test page",
		Products: products,
	}
	resp.WriteHeader(200)
	tmpt.ExecuteTemplate(resp, "Index", &wd)
}

func New(resp http.ResponseWriter, req *http.Request) {
	wd := model.WebData{
		Title: "New product",
	}
	resp.WriteHeader(200)
	tmpt.ExecuteTemplate(resp, "New", wd)
}

func Insert(resp http.ResponseWriter, req *http.Request) {
	if req.Method == "POST" {
		name := req.FormValue("name")
		description := req.FormValue("description")
		price := req.FormValue("price")
		amount := req.FormValue("amount")

		priceOk, err := strconv.ParseFloat(price, 64)
		if err != nil {
			panic(err.Error())
			resp.WriteHeader(500)
		}

		amountOk, err := strconv.Atoi(amount)
		if err != nil {
			panic(err.Error())
			resp.WriteHeader(500)
		}

		database.InsertProduct(name, description, priceOk, amountOk)
	}
	http.Redirect(resp, req, "/", 301)
}

func Delete(resp http.ResponseWriter, req *http.Request) {
	idQuery, err := strconv.Atoi(req.URL.Query().Get("id"))
	if err != nil {
		panic(err.Error())
	}
	database.DeleteProduct(idQuery)
	http.Redirect(resp, req, "/", 301)
}

func Edit(resp http.ResponseWriter, req *http.Request) {
	idQuery := req.URL.Query().Get("id")
	product := database.GetProduct(idQuery)
	products := []model.Product{}
	products = append(products, product)

	wd := model.WebData{
		Title:    "Edit product",
		Products: products,
	}
	tmpt.ExecuteTemplate(resp, "Edit", wd)
}

func Edited(resp http.ResponseWriter, req *http.Request) {
	if req.Method == "POST" {
		code := req.FormValue("code")
		name := req.FormValue("name")
		description := req.FormValue("description")
		price := req.FormValue("price")
		amount := req.FormValue("amount")

		codeOk, err := strconv.Atoi(code)
		if err != nil {
			panic(err.Error())
			resp.WriteHeader(500)
		}

		priceOk, err := strconv.ParseFloat(price, 64)
		if err != nil {
			panic(err.Error())
			resp.WriteHeader(500)
		}

		amountOk, err := strconv.Atoi(amount)
		if err != nil {
			panic(err.Error())
			resp.WriteHeader(500)
		}

		database.EditProduct(codeOk, name, description, priceOk, amountOk)
	}
	http.Redirect(resp, req, "/", 301)
}
