package database

import (
	"crud/config"
	"crud/model"
)

func GetProducts() []model.Product {
	db := config.Connector()
	defer db.Close()

	getProducts, err := db.Query("SELECT * from products")

	if err != nil {
		panic(err.Error())
	}

	products := []model.Product{}

	for getProducts.Next() {
		p := model.Product{}

		err = getProducts.Scan(&p.Code, &p.Product, &p.Description, &p.Price, &p.Amount)
		if err != nil {
			panic(err.Error())
		}

		products = append(products, p)
	}

	return products
}

func InsertProduct(PRODUCT string, DESCRIPTION string, PRICE float64, AMOUNT int) {

	db := config.Connector()
	defer db.Close()

	insertProduct, err := db.Prepare("INSERT INTO products(PRODUCT, DESCRIPTION, PRICE, AMOUNT) VALUES ($1, $2, $3, $4)")
	if err != nil {
		panic(err.Error())
	}
	insertProduct.Exec(PRODUCT, DESCRIPTION, PRICE, AMOUNT)
}

func EditProduct(code int, product string, description string, price float64, amount int) {

	db := config.Connector()
	defer db.Close()

	updateProduct, err := db.Prepare("UPDATE products SET PRODUCT = $2, DESCRIPTION = $3, PRICE = $4, AMOUNT = $5 Where Code = $1")
	if err != nil {
		panic(err.Error())
	}
	updateProduct.Exec(code, product, description, price, amount)
}

func DeleteProduct(i int) {
	db := config.Connector()
	defer db.Close()

	deleteProduct, err := db.Prepare("DELETE FROM products WHERE CODE = $1")
	if err != nil {
		panic(err.Error())
	}

	deleteProduct.Exec(i)
}

func GetProduct(i string) model.Product {
	db := config.Connector()
	defer db.Close()

	p := model.Product{}

	getProduct, err := db.Query("SELECT * FROM products WHERE code = $1", i)
	if err != nil {
		panic(err.Error())
	}

	for getProduct.Next() {
		err = getProduct.Scan(&p.Code, &p.Product, &p.Description, &p.Price, &p.Amount)
		if err != nil {
			panic(err.Error())
		}
	}

	return p
}
