package router

import (
	"crud/controller"
	"net/http"
)

func SendRoute() {
	http.HandleFunc("/", controller.Index)
	http.HandleFunc("/New", controller.New)
	http.HandleFunc("/insert", controller.Insert)
	http.HandleFunc("/delete", controller.Delete)
	http.HandleFunc("/Edit", controller.Edit)
	http.HandleFunc("/edited", controller.Edited)
}
