package config

import (
	"database/sql"

	"crud/util"

	_ "github.com/joho/godotenv"
	_ "github.com/lib/pq"
)

func Connector() *sql.DB {
	conn := "postgres://" + util.GetEnvVariable("USER_DB") + ":" + util.GetEnvVariable("PASS_DB") + "@" + util.GetEnvVariable("ADDRESS_DB") + "/" + util.GetEnvVariable("DB") + "?sslmode=disable"
	db, err := sql.Open("postgres", conn)

	if err != nil {
		panic(err.Error())
	}

	return db
}
