package model

type WebData struct {
	Title    string
	Products []Product
}
