package model

type Product struct {
	Code        int
	Product     string
	Description string
	Price       float64
	Amount      int
}
